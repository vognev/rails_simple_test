class Flyer < ActiveRecord::Base

  validates :title, length: { minimum: 5 }

  def full_name
    "#{title}: #{page_count} pages"
  end
end
