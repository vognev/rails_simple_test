require 'test_helper'

describe Flyer do
  it 'has 2 object from fixtures' do
    Flyer.count.must_equal 3
  end

  it 'title should be more then 5 symbols' do
    Flyer.new(title: 'abcde').must_be :valid?
  end
end
