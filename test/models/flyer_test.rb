require 'test_helper'

class FlyerTest < ActiveSupport::TestCase
  def test_fixtures
    assert_equal 3, Flyer.count
  end

  def test_bullshit
    flyer = flyers(:one)
    assert_equal 'Flyer about Bullshit1', flyer.title
    assert_equal 10, flyer.page_count.to_i
  end

  def test_flyer_full_title
    flyer = flyers(:one)
    assert_equal flyer.full_name, "Flyer about Bullshit1: 10 pages"
  end
end
